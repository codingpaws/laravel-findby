<?php

namespace CodingPaws\FindBy\Tests;

use Illuminate\Database\QueryException;
use Orchestra\Testbench\TestCase;

class FeatureTest extends TestCase
{
    public function testSavingNotImpaired()
    {
        $example = new Example();
        $this->expectException(QueryException::class);
        $example->save();
    }
}
